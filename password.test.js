const { checkLenght, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLenght('12345678')).toBe(true)
  })
  test('should 7 characters to be false', () => {
    expect(checkLenght('1234567')).toBe(false)
  })
  test('should 25 characters to be true', () => {
    expect(checkLenght('1111111111111111111111111')).toBe(true)
  })
  test('should 26 characters to be false', () => {
    expect(checkLenght('11111111111111111111111111')).toBe(false)
  })
})
describe('Test Alphabet in password', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit in password', () => {
  test('should has digit 0-9 in password', () => {
    expect(checkDigit('0')).toEqual(true)
  })
  test('should has not  digit 0-9 in password', () => {
    expect(checkDigit('ssss')).toEqual(false)
  })
})


describe('Test symbol in password', () => {
  test('should has symbol  ! in password to be true ', () => {
    expect(checkSymbol('!kkk')).toEqual(true)
  })
  test('should has symbol  _ in password to be true ', () => {
    expect(checkSymbol('_kkk')).toEqual(true)
  })
  test('should has not symbol in password', () => {
    expect(checkSymbol('bdx')).toEqual(false)
  })
})

describe('Test password', () => {
  test('should password #abc1234 to be true ', () => {
    expect(checkPassword('#abc1234')).toEqual(true)
  })
  test('should password 1234 to be false ', () => {
    expect(checkPassword('1234')).toEqual(false)
  })
  test('should password cde@fgh to be false ', () => {
    expect(checkPassword('cde@fgh')).toEqual(false)
  })
  test('should password 12345678 to be false ', () => {
    expect(checkPassword('12345678')).toEqual(false)
  })
  test('should password Mod@12 to be false ', () => {
    expect(checkPassword('Mod@12')).toEqual(false)
  })
  test('should password !@#$% to be false ', () => {
    expect(checkPassword('!@#$%')).toEqual(false)
  })
})

