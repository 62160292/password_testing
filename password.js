const checkLenght = function (password) {
  return password.length >= 8 && password.length <= 25
}
const checkAlphabet = function (password) {
  return /[a-zA-z]/.test(password)
}

const checkDigit = function (password) {
  return /[0-9]/.test(password)
}

const checkSymbol = function (password) {
  const symbols = '!"#$%&()*+,-./:;<=>?@[]^_`{|}~'
  for (const ch of password) {
    if (symbols.includes(ch.toLowerCase())) return true
  }
  return false

}

const checkPassword = function (password) {
  return checkLenght(password) && checkAlphabet(password) && checkDigit(password) && checkSymbol(password)
}

module.exports = {
  checkLenght,
  checkAlphabet,
  checkDigit,
  checkSymbol,
  checkPassword
}
